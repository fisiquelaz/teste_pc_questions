import { Task } from './task';

/** Mode the user can interact with a choice question.
 * In 'oneshot' mode, the user can choose a single option.
 * In 'manyshot' mode, the user can choose multiple options.
 * In 'unsure' mode, the user can give weight to multiple options.
 */
export type ChoiceMode = 'oneshot' | 'manyshots' | 'unsure'

/**
 * The options shown to the user may be layered in vertical or horizontal.
 */
export type ChoiceLayout = 'vert' | 'horiz'

/**
 * When using 'unsure' mode, each option has a degree of "certainity", which value
 * is in the range [0-1].
 */
export interface UnsureOption {
  answerIndex: number
  certainty: number
}

/**
 * The result of a choice question can be a number (index of the option), a number
 * array (indexes of the chosen options), or a UnsureOption array (where each choosen
 * option has a certainity degree associated).
 */
export type ChoiceResult = number | number[] | UnsureOption[]

/**
 * An option is comprised of its statement (text in markdown format) and two optional
 * booleans for handling user feedback for an option selected by the user. When it's neither
 * correct or wrong, it means that the user didn't chose it.
 */
export interface Option {
  text: string   // markdown text
  correct?: boolean
  wrong?: boolean
}

/**
 * A choice question has a set of options, the possible right answers, and a layout, which
 * is used to dispose the options (vertical or horizontal aligned).
 */
export class ChoiceTask extends Task {
  options: Option[];
  rightAnswer: number
  layout: ChoiceLayout

  constructor(options: any = {}) {
    super(options)
    this.type = 'choice'
    this.options = options.options || ['']
    this.layout = options.layout || 'vert'
  }
}
