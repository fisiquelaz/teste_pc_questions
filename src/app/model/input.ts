import { Task, Result } from './task';

/**
 * The result of a input task is the string entred by the user.
 */
export type InputResult = string

/**
 * An input question has a right answer or a set of possible right answers.
 */
export class InputTask extends Task {
  rightAnswer: string | string[]

  constructor(options: any = {}) {
    super(options)
    this.type = 'input'
    // this.rightAnswer = options.rightAnswer
  }
}
