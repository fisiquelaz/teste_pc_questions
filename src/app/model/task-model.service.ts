import { Injectable } from '@angular/core'
import { Task } from './task'
import { ChoiceTask } from './choice'
import { InputTask } from './input'

import { ExamService } from '../routes/exam/exam.service'
import lgTasks from './tasks-logic.json'
import blTasks from './tasks-block.json'
import cdTasks from './tasks-code.json'
import tstTasks from './tasks-tests.json'


/**
 * Function to map a object read from a JSON file into a Task object.
 * A task subclass is used according to the task type.
 * @param task Object read from a JSON file
 */
const jsonElmToTaskObj = (task: Task) => {
  switch (task.type) {
    case 'input': return new InputTask(task)
    case 'choice': return new ChoiceTask(task)
  }
}
const blockTasks = blTasks.map(jsonElmToTaskObj)
const logicTasks = lgTasks.map(jsonElmToTaskObj)
const codeTasks = cdTasks.map(jsonElmToTaskObj)
const testTasks = tstTasks.map(jsonElmToTaskObj)


/**
 * All tasks of an exam, grouped in stages. Each stage has a percent time of the whole exam time.
 */
const tasksSet = {
  // tasks: [ logicTasks, blockTasks, codeTasks ],
  tasks: [ testTasks ],
  percentTime: [ 0.5, 0.8, 1.0 ]
}

/**
 * Injectable service responsible for getting the next task when the user answer the current
 * one or when s/he passes it. The tasks are grouped in stages and each one has a maximum time to
 * answers its tasks. For instance, When a stage has 0.5 percent time, it means that after half time
 * of the whole exam has passed, the next task will be get from a next stage (i.e. the user goes to
 * the next stage) even if the user has not answered all the tasks of the current one.
 */
@Injectable({
  providedIn: 'root'
})
export class TaskModelService {

  /** current stage */
  private currSet: number

  constructor(private exam: ExamService) {
    this.currSet = 0
  }

  next(): Task {
    // if the time for the current set has expired, go to the next one
    const ellapsedPercentTime = this.exam.currentTime / this.exam.timeLimit
    if (ellapsedPercentTime > tasksSet.percentTime[this.currSet]) {
      this.currSet++
    }

    let tasks: Task[]
    let task: Task
    while (!task && this.currSet < tasksSet.tasks.length) {
      tasks = tasksSet.tasks[this.currSet]
      // find a task which is not in the user's results (so it was not done yet)
      task = tasks.find(t => {
        const userTask = this.exam.answeredTasks.find(taskId => taskId === t.id )
        return userTask === undefined
      })
      if (!task) {
        this.currSet++
      }
    }
    return task
  }
}
