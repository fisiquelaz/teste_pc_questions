
/**
 * Type of tasks the application can handle
 */
export type TaskType = 'choice' | 'input'

/**
 * When a task is closed, there are three possible outcomes. 1) it could be answered be the user,
 * 2) the time for the task could be experied (timeout), or the user could skiped the task.
 */
export enum ResultType {
  ANSWERED,
  TIMEOUT,
  SKIPPED
}

/**
 * Data result of a answered task.
 */
export interface Result {
  resultType: ResultType // what happened?
  starttime: Date        // when the user started?
  ellapsedTime: number   // how long s/he took in seconds?
  answer?: any           // what was the answer?
  success?: boolean      // did s/he succeed?
}

// global var used to create unique ids
let id = 0

/**
 * General abstract class for representing tasks.
 */
export abstract class Task {
  id: string
  type: TaskType
  title = 'Sem título'
  skills: string[] = []
  difficulty = 0
  statement = 'Sem enunciado'  // markdown string
  timeLimit?: number  // is there a time limit to answer the task?
  rightAnswer: any    // the right answer depends on the type of the task

  constructor(options: any) {
    this.id = (++id).toString()
    // tslint:disable:curly
    if ('title' in options) this.title = options.title
    if ('skills' in options) this.skills = options.skills
    if ('difficulty' in options) this.difficulty = options.difficulty
    if ('statement' in options) this.statement = options.statement
    if ('timeLimit' in options) this.timeLimit = options.timeLimit
    if ('rightAnswer' in options) this.rightAnswer = options.rightAnswer
  }
}

