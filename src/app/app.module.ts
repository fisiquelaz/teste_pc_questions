import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'

// tslint:disable-next-line:max-line-length
import { MatButtonModule, MatIconModule, MatProgressBarModule, MatCardModule, MatRadioModule, MatSlideToggleModule, MatDividerModule, MatGridListModule, MatFormFieldModule, MatInputModule, MatTooltipModule, MatCheckboxModule } from '@angular/material'
import { MarkdownModule } from 'ngx-markdown'
import { AngularFireModule } from '@angular/fire'
import { AngularFirestoreModule } from '@angular/fire/firestore'

import { routes } from './routes/routes'
import { IdentificationComponent } from './routes/identification/identification.component'
import { ExamComponent } from './routes/exam/exam.component'
import { ResultComponent } from './routes/result/result.component'
import { RulesComponent } from './routes/rules/rules.component'
import { PageNotFoundComponent } from './routes/page-not-found/page-not-found.component'

import { QuestionComponent } from './components/question/question.component'
import { TaskComponent } from './components/task/task.component'
import { TimePipe } from './time.pipe'
import { environment } from 'src/environments/environment'
import { AppComponent } from './app.component';
import { InputComponent } from './components/input/input.component';

import { NgxBlocklyModule } from 'ngx-blockly';



@NgModule({
  declarations: [
    AppComponent,
    QuestionComponent,
    TaskComponent,
    InputComponent,
    IdentificationComponent,
    ExamComponent,
    ResultComponent,
    PageNotFoundComponent,
    TimePipe,
    RulesComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatProgressBarModule,
    MatCardModule,
    MatRadioModule,
    MatSlideToggleModule,
    MatDividerModule,
    MatGridListModule,
    MatTooltipModule,
    MatCheckboxModule,
    MarkdownModule.forRoot(),
    RouterModule.forRoot(routes),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    NgxBlocklyModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
