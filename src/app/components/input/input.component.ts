import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, OnChanges, SimpleChanges } from '@angular/core'
import { InputTask, InputResult } from '../../model/input'

/**
 * Component responsible to get textual answer from the user.
 */
@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit, OnChanges {
  @Input() question: InputTask
  @Output() answered = new EventEmitter<InputResult>()
  @ViewChild('answerInput') input: ElementRef
  answer: string

  constructor() {
    this.question = new InputTask()
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.question) {
      this.answer = ''
      this.input.nativeElement.focus()
    }
  }

  submit() {
    this.answered.emit(this.answer.toLocaleUpperCase())
  }
}
