import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core'
import {trigger, state, style, animate, transition } from '@angular/animations'
import { Task, Result, ResultType } from '../../model/task'

/**
 * Component responsible for presenting a task to the user according to the task type.
 */
@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
  animations: [
    trigger('fadeOutIn', [
      state('shown', style({ opacity: 1 })),
      state('hidden', style({ opacity: 0 })),
      transition('* => *', [ animate('0.5s') ])
    ])
  ]
})
export class TaskComponent implements OnInit, OnChanges {
  @Input() task: Task
  @Input() fade: boolean
  @Output() concluded = new EventEmitter<Result>()
  isShown = true
  isAnswered = false
  startTime: number

  constructor() { }

  ngOnInit() {
    this.fade = this.fade || true
    // TODO check if the task attribute has not been set
  }

  /**
   * Monitor when a new task must be shown and starts the task.
   * @param changes Changes in the component attributes.
   */
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.task) {
      if (changes.task.firstChange) {
        this.startTime = Date.now()
      } else {
        if (this.fade) {
          this.task = changes.task.previousValue
          this.isShown = false
          // wait the conclusion of the fadein to start the new task.
          setTimeout(() => {
            this.startTask(changes.task.currentValue)
          }, 500);
        } else {
          this.startTask()
        }
      }
    }
  }

  /**
   * Start running a new task.
   * @param task Task to start.
   */
  private startTask(task?: Task) {
    this.task = task || this.task
    this.isShown = true
    this.isAnswered = false
    this.startTime = Date.now()
  }

  reset() {
    // const zeroArray = () => new Array(this.question.options.length).fill(0)
    // this.status = []
    // this.confidence = zeroArray()
    // this.shots = this.mode === 'unsure' ? zeroArray() : []
    // this.answered = false
    // check if we can call defineViewOrder() here
  }

  /**
   * Called when the user answer a task.
   * @param answer The user answer
   */
  answered(answer) {
    this.isAnswered = true
    this.concluded.emit({
      // id: this.task.id,
      resultType: ResultType.ANSWERED,
      starttime: new Date(),
      ellapsedTime: Date.now() - this.startTime,  // ellapsed time
      answer
    })
  }

  /**
   * Called when the user skips a task.
   */
  skipped() {
    this.isAnswered = true
    this.concluded.emit({
      // id: this.task.id,
      resultType: ResultType.SKIPPED,
      starttime: new Date(),
      ellapsedTime: Date.now() - this.startTime,  // ellapsed time
    })
  }

}
