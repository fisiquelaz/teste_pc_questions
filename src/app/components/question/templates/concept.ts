import { ChoiceTask, Option } from '../../../model/choice';

const keywords = [
  {
    keyword: '`if`',
    goal: 'alterar o fluxo de execução do programa em função de uma condição (expressão lógica) verdadeira',
    usage: {
      do: [
`\`\`\`
if (x == 5) {
  // ...
}
\`\`\``,
      ],
      dont: [
`\`\`\`
if x == 5 {
  // ...
}
\`\`\``,
`\`\`\`
if x is 5:
  // ...
\`\`\``,
`\`\`\`
if (x == 5) then {
  // ...
}
\`\`\``,
`\`\`\`
if (x == 5):
  // ...
\`\`\``,
`\`\`\`
if (x == 5) then {
  // ...
}
\`\`\``,
      ]
    }
  },
  {
    keyword: '`else`',
    goal: 'definir o fluxo do programa quando a condição (expressão lógica) do `if` for falsa'
  },
  {
    keyword: '`while`',
    goal: 'executar um bloco de comandos repetidamente enquanto uma condição for verdadeira'
  },
  {
    keyword: '`for`',
    goal: 'executar um bloco de comandos repetidamente com incremento ou decremento de valores'
  },
]

const random = (min: number, max: number) => Math.floor(Math.random() * (max - min)) + min
const swap = (array: any[], i: number, j: number) => {
  const aux = array[i]
  array[i] = array[j]
  array[j] = aux
  return array
}

export class Concept {
  constructor() {
  }

  static getKeywordGoalQuestion(numOptions: number = 4): ChoiceTask {
    const indexes = keywords.map((_, index) => index)   // returns [0, 1, 2, 3,...]
    const options: Option[] = []
    for (let i = 0; i < numOptions; i++) {
      swap(indexes, i, random(i, numOptions))
      const key = keywords[indexes[i]]
      options.push({
        text: key.goal
      })
    }
    const rightAnswer = 0
    const elm = keywords[indexes[rightAnswer]]
    return new ChoiceTask({
      statement: `Qual o objetivo do comando ${elm.keyword}?`,
      options,
      rightAnswer
    })
  }
}
