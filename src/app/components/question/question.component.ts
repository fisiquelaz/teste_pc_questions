import { Component, OnInit, Input, Output, EventEmitter, Attribute, OnChanges, SimpleChanges } from '@angular/core';
import { ChoiceTask, ChoiceMode, ChoiceResult, ChoiceLayout } from '../../model/choice';
import { ExamService } from 'src/app/routes/exam/exam.service';

type Status = 'correct' | 'wrong'

/**
 * Component responsible for getting the chosen option of the user.
 */
@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit, OnChanges {
  @Input() question: ChoiceTask
  @Output() answered = new EventEmitter<ChoiceResult>()

  isShown = true
  layout: ChoiceLayout
  mode: ChoiceMode

  private status: Status[] = []
  private shots: number[] = []
  private confidence: number[] = []
  private _answered = false
  viewOrder: number[] = []


  constructor(private exam: ExamService) {
    this.question = new ChoiceTask()
  }

  private defineViewOrder() {
    if (this.question) {
      this.viewOrder = this.question.options.map((_, i) => i)  // pick all index in order
      if (this.exam.shuffleOptions) {
        this.viewOrder.sort(() => Math.random() - 0.5)  // sort in a random way, i.e. it shuffles
      }
    }
  }

  private getRightOptionIndex(index: number) {
    return this.viewOrder[index]
  }

  ngOnInit() {
    this.mode = this.exam.choiceMode || 'oneshot'
    this.layout = this.question.layout || 'vert'   // defaults to options vertically aligned
    this.defineViewOrder()
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.reset();
    if (changes.question && !changes.question.firstChange) {
      // this.question = changes.question.previousValue
      // this.isShown = false
      // setTimeout(() => {
        // this.question = changes.question.currentValue
        this.defineViewOrder()
        // this.isShown = true
      // }, 500);
    }
  }

  reset() {
    const zeroArray = () => this.question ? new Array(this.question.options.length).fill(0) : []
    this.status = []
    this.confidence = zeroArray()
    this.shots = this.exam.choiceMode === 'unsure' ? zeroArray() : []
    this._answered = false
    // check if we can call defineViewOrder() here
  }

  updateStatus(viewIndex: number, showCorrect: boolean = true) {
    if (this.exam.showAnswers)  {
      this.status[viewIndex] = 'wrong'
      if (showCorrect) {
        this.status[this.question.rightAnswer] = 'correct'
      }
    }
  }

  answer(answerIndex: number) {
    const handler = this.mode + 'Answer'
    this[handler].call(this, answerIndex)  // call the method according to the mode (oneshotAnswer(), manyshotsAnswer() or unsureAnswer())
  }

  oneshotAnswer(answerIndex: number) {
    this.updateStatus(answerIndex)
    // this.status[] = 'wrong'
    // this.status[this.question.rightAnswer] = 'correct'
    this.answered.emit(answerIndex)
    //   this.answered.emit({
    //   id: '1',
    //   type: ResultType.ANSWERED,
    //   ellapsedTime: 1,
    //   answer: answerIndex
    // })
    this._answered = true
  }

  manyshotsAnswer(answerIndex: number) {
    if (this.status[answerIndex] === undefined) {
      this.shots.push(answerIndex)
      if (answerIndex === this.question.rightAnswer) {
        this.updateStatus(answerIndex)
        // this.status[answerIndex] = 'correct'
        this.answered.emit(this.shots)
        // this.answered.emit({
        //   id: '1',
        //   type: ResultType.ANSWERED,
        //   ellapsedTime: 1,
        //   answer: this.shots
        // })
        this._answered = true
      } else {
        this.status[answerIndex] = 'wrong'
        this.updateStatus(answerIndex, false)
        // this.status[answerIndex] = 'wrong'
      }
    }
  }

  unsureAnswer(answerIndex: number) {
    console.log(answerIndex);
    console.log(this.shots);

    this.shots[answerIndex]++
    const count = this.shots.reduce((prev, curr) => prev + curr, 0)  // sum of confidences
    this.confidence = this.shots.map(num => 100 * num / count) // percentage of each one
  }

  unsureConfirm() {
    this.updateStatus(this.question.rightAnswer)
    // this.status[this.question.rightAnswer] = 'correct'
    this.answered.emit(this.confidence)
    // this.answered.emit({
    //   id: '1',
    //   type: ResultType.ANSWERED,
    //   ellapsedTime: 1,
    //   answer: this.confidence
    // })
    this._answered = true
  }

}
