import { Routes } from '@angular/router';
import { IdentificationComponent } from '../routes/identification/identification.component'
import { AccessGuard } from './access.guard'
import { ExamComponent } from '../routes/exam/exam.component'
import { ResultComponent } from '../routes/result/result.component'
import { RulesComponent } from './rules/rules.component';
import { PageNotFoundComponent } from '../routes/page-not-found/page-not-found.component'

/**
 * Routes of the application.
 */
export const routes: Routes = [
  //{ path: 'id', component: IdentificationComponent, canActivate: [AccessGuard] },
  //{ path: 'rules', component: RulesComponent, canActivate: [AccessGuard] },
  { path: 'exam', component: ExamComponent, canActivate: [AccessGuard] },
  { path: 'result', component: ResultComponent, canActivate: [AccessGuard] },
  { path: '', redirectTo: 'exam', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
]

