import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { ExamService } from './exam.service'
import { Subscription } from 'rxjs'
import { TaskModelService } from '../../model/task-model.service'
import { Task, Result } from '../../model/task'
import { XmlBlock, NgxBlocklyConfig, NgxBlocklyGeneratorConfig, NgxToolboxBuilderService, LOGIC_CATEGORY, LOOP_CATEGORY, MATH_CATEGORY, TEXT_CATEGORY, LISTS_CATEGORY, COLOUR_CATEGORY, VARIABLES_CATEGORY, FUNCTIONS_CATEGORY } from 'ngx-blockly';

/**
 * Main page during the exam (the user answering the tasks).
 */
@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.scss']
})
export class ExamComponent implements OnInit {
  testblock = new XmlBlock('controls_if');
  public config1: NgxBlocklyConfig = {
    toolbox: '<xml id="toolbox" style="display: none">' +
            '<block type="controls_if"></block>' +
            '<block type="controls_repeat_ext"></block>' +
            '<block type="logic_compare"></block>' +
            '<block type="math_number"></block>' +
            '<block type="math_arithmetic"></block>' +
            '<block type="text"></block>' +
            '<block type="text_print"></block>' +
            '</xml>',
      scrollbars: false,
      trashcan: true,
      sounds: false,
      readonly: true
  };

  public generatorConfig: NgxBlocklyGeneratorConfig = {
          python: true,
  };

  onCode(code: string) {
      console.log(code);
  }

  isGretting = false
  task: Task

  current = 0
  answered = false
  isShown = true
  mode = 'oneshot'

  progressSubscription: Subscription

  constructor(public exam: ExamService, private taskModel: TaskModelService, private router: Router, ngxTB1: NgxToolboxBuilderService) {
        /*ngxTB1.categories = [
                   LOGIC_CATEGORY,
                   LOOP_CATEGORY,
                   MATH_CATEGORY,
                   TEXT_CATEGORY,
                   LISTS_CATEGORY,
                   COLOUR_CATEGORY,
                   VARIABLES_CATEGORY,
                   FUNCTIONS_CATEGORY
        ];
        this.config1.toolbox = ngxTB1.build();*/
  }

  ngOnInit() {
    if (!this.exam.isInitialized()) {
      this.router.navigate(['/'])
    } else {
      if (this.exam.isStarted()) {
        this.isGretting = false
        this.next()
        // this.task = this.taskModel.next()
      }
      else {
        setTimeout(() => {
          this.isGretting = false
          this.next()
          this.exam.startExam()
          this.exam.ellapsedProgress$.subscribe((progress: number) => {
            if (progress >= 100) {
              this.router.navigate(['/result'])
            }
          })
        }, 1000)
      }
    }
  }

  /**
   * Called when a task is conclused (by the user answering, skipping it or by timeout).
   * @param result The answer given by the user
   */
  concluded(result: Result) {
    this.answered = true
    this.exam.concludeTask(this.task, result)
    // if (!this.showAnswers) {
      this.next()
    // }
  }

  /**
   * Check if a next task must be shown or if the exam has finished (going the result page).
   */
  next() {
    this.task = this.taskModel.next()
    if (this.task) {
      this.answered = false
      this.isShown = false
    } else {
      // there is no more question to ask
      this.router.navigate(['/result'])
    }
  }

}
