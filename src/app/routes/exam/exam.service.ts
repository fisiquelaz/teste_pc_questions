import { Injectable } from '@angular/core'
import { Subject, interval, Subscription } from 'rxjs'
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore'

import { ChoiceMode } from '../../model/choice'
import { Task, Result } from '../../model/task';

export interface ExamConfig {
  timeLimit?: number
  showAnswers?: boolean
  choiceMode?: ChoiceMode
  shuffleOptions?: boolean

  // ...
}

/**
 * Injectable service to control the exam (when it starts, when it finishes, when a task is
 * concluded) and collect the task answers.
 */
@Injectable({
  providedIn: 'root'
})
export class ExamService {

  private config: ExamConfig = {}

  private _id = ''
  private _classId = ''
  private _hasReadObs = false
  private _isStarted = false

  answeredTasks: string[] = []

  private _answersCollection: AngularFirestoreCollection
  private _examsCollection: AngularFirestoreCollection

  private _ellapsedTime = 0

  private _progressSubscription: Subscription
  private _ellapsedProgress = new Subject<number>();
  ellapsedProgress$ = this._ellapsedProgress.asObservable();

  constructor(private db: AngularFirestore) {
    this.answeredTasks = []
    this._answersCollection = this.db.collection('answers')
    this._examsCollection = this.db.collection('exams')
  }

  /** Save the exam config to use when it starts. */
  init(enrollmentId: string, classId: string, config?: ExamConfig) {
    this._id = enrollmentId
    this._classId = classId
    this.config = {
      timeLimit: 90 * 60,      // defaults to 90 minutes,
      choiceMode: 'oneshot',   // defaults to 'oneshot' in choice tasks
      ...config
    }
  }

  /**
   * Set an internal flag indicating that the user has alread read the exam observations.
   */
  readObs() { this._hasReadObs = true }

  get id() { return this._id }
  get timeLimit() { return this.config.timeLimit }
  get currentTime() { return this._ellapsedTime }
  get missingTime() { return this.timeLimit - this.currentTime }
  get choiceMode() { return this.config.choiceMode }
  get shuffleOptions() { return this.config.shuffleOptions }
  get showAnswers() { return this.config.showAnswers }
  isInitialized() { return this._id !== '' }
  isStarted() { return this._isStarted }
  hasReadObs() { return this._hasReadObs }

  /**
   * Start the exam. The time starts counting.
   */
  startExam() {
    // starts only if it was initialized and has not started yet
    if (this.isInitialized() && this.currentTime === 0) {
      this._examsCollection.add({
        enrollmentId: this._id,
        class: this._classId,
        starttime: new Date(),
      })
      this._isStarted = true
      this._ellapsedTime = 0
      this._progressSubscription = interval(1000).subscribe(seconds => {
        this._ellapsedTime = seconds + 1
        this._ellapsedProgress.next(100 * this._ellapsedTime / this.timeLimit)
        if (this._ellapsedTime >= this.timeLimit) {
          this.finishExam()
          // TODO emit concluded exam event
        }
      })
    }
  }

  /**
   * Conclude the exam.
   */
  finishExam() {
    if (this._progressSubscription) {
      this._progressSubscription.unsubscribe()
      this._ellapsedTime = 0
    }
    this._isStarted = false
    this._id = ''
  }

  /**
   * Called when a task has finished either by the user answering, or because the time is over,
   * or because the user skipped the task.
   * @param task The task answered by the user
   * @param result The answer given by the user
   */
  concludeTask(task: Task, result: Result) {
    this._answersCollection.add({
      enrollmentId: this._id,
      task: task.id,
      success: task.rightAnswer === result.answer,
      ...result
    })
    this.answeredTasks.push(task.id)
  }
}
