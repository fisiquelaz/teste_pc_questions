import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ExamService } from '../exam/exam.service';

/**
 * Before starting the exam, this page is shown. All the observations for the exam are presented and should be confirmed by the user.
 */
@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.scss']
})
export class RulesComponent implements OnInit {

  observations: string[]
  hasRead: boolean

  constructor(private exam: ExamService, private router: Router) {
    const duration = Math.floor(this.exam.timeLimit / 60)  // seconds to minutes
    this.observations = [
      `Esta avaliação tem como objetivo identificar pontos fortes e fracos a fim de melhor determinar nossa atuação como professor.
       Ela não possui uma nota associada. Então, fique tranquilo! Porém, faça o melhor que puder.`,
      `A avaliação tem uma duração de até ${duration} minutos. Uma barra de progresso na parte superior da página indicará o tempo
       decorrido. Após o tempo máximo, não será mais possível enviar respostas.`,
      `Não há um número fixo de questões. Elas vão aparecer conforme você for respondendo-as. Faça as que puder dentro do tempo
       máximo estipulado.`,
      `É uma avaliação diagnóstica. Portanto, não vale a pena consultar outras pessoas ou qualquer tipo de material online ou offline.`,
      `É permitido e fortemente aconselhado usar uma folha em branco como rascunho. Retire uma folha em branco do seu caderno para
       servir de rascunho.`,
      `Leia com calma e atenção o enunciado das questões. Muitas vezes, erra-se uma questão devido à pressa na leitura e, por
       consequência, à má interpretação do enunciado. Se necessário, releia o texto.`,
      `Em questões cuja resposta é um texto, fique atento ao formato de resposta solicitado (ex: uso do vírgula, espaço em branco
       etc). Suas respostas serão comparadas com textos esperados no formato especificado. Se você errar no formato, errará a questão.`,
      `Não saber é melhor que saber errado. Portanto, no caso em que você não tem a mínima ideia da resposta, é melhor pular a questão.`,
    ]
  }

  ngOnInit() {
    if (!this.exam.isInitialized()) {
      this.router.navigate(['/'])
    }
    else if (this.exam.isStarted()) {
      this.router.navigate(['/exam'])
    }
  }

  start() {
    this.router.navigate(['/exam'])
  }

}
