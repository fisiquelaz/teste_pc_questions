import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { AngularFirestore } from '@angular/fire/firestore'
import { take } from 'rxjs/operators'

import { ExamService, ExamConfig } from '../exam/exam.service'

const ACCESS_PASS = 'NOVOS'

/** Exam configuration */
const config: ExamConfig = {
  choiceMode: 'manyshots',
  shuffleOptions: false
}

/**
 * Initial page, where the user gives his/her credentials. It validates the code for a specific class and grant access
 * only in the class timetable.
 */
@Component({
  selector: 'app-identification',
  templateUrl: './identification.component.html',
  styleUrls: ['./identification.component.scss']
})
export class IdentificationComponent implements OnInit {
  subject = 'Pensamento Computacional'
  avalName = 'Avaliação diagnóstica 2019'
  enrollmentId: string
  accessCode: string
  invalidAccessCode: boolean

  classes: any

  constructor(private exam: ExamService, private router: Router, private db: AngularFirestore) {
  }

  ngOnInit() {
    if (this.exam.isInitialized()) {
      this.router.navigate(['/rules'])
    }
    this.invalidAccessCode = false
  }

  private timetableToNumber(timetable: string) {
    const time = timetable.substr(2)
    switch (time) {
      case 'M12': return 0;
      case 'M34': return 1;
      case 'M56': return 2;
      case 'T12': return 3;
      case 'T34': return 4;
      case 'T56': return 5;
      case 'N12': return 6;
      case 'N34': return 7;
      default: return 8;
    }
  }

  private timetableToTime(timetable: string) {
    const time = timetable.substr(2)
    switch (time) {
      case 'M12': return { from: { h:  7, m:  0 }, to: { h:  8, m: 40 } };
      case 'M34': return { from: { h:  8, m: 55 }, to: { h: 10, m: 35 } };
      case 'M56': return { from: { h: 10, m: 50 }, to: { h: 12, m: 30 } };
      case 'T12': return { from: { h: 13, m:  0 }, to: { h: 14, m: 40 } };
      case 'T34': return { from: { h: 14, m: 55 }, to: { h: 16, m: 35 } };
      case 'T56': return { from: { h: 16, m: 50 }, to: { h: 18, m: 30 } };
      case 'N12': return { from: { h: 19, m:  0 }, to: { h: 20, m: 40 } };
      case 'N34': return { from: { h: 20, m: 55 }, to: { h: 22, m: 35 } };
      default:    return { from: { h:  0, m:  0 }, to: { h: 23, m: 59 } };
    }
  }

  private classToChar(className: string) {
    const charCode = className.charCodeAt(0)
    const num = Number.parseInt(className.substr(className.length - 1), 10)
    return String.fromCharCode(charCode + num)

  }

  init() {
    if (this.accessCode === ACCESS_PASS) {
      this.exam.init(this.enrollmentId, 'TEST', config)
      this.router.navigate(['/rules'])
    }
    else {
      this.db.collection('classes')
        .valueChanges()
        .pipe(take(1))
        .subscribe(classes => {
          let classId
          const found: any = classes.find((c: any) => {
            classId = c.class
            const c1 = this.classToChar(c.class)
            const c2 = this.timetableToNumber(c.timetable)
            const c3 = String.fromCharCode(c1.charCodeAt(0) + c2)
            const code = c1 + c2 + c3
            return code === this.accessCode
          })
          if (found) {
            const now = new Date()
            const weekday = (now.getDay() + 1).toString()
            if (weekday === found.timetable[0] || weekday === found.timetable[1]) {
              const interval = this.timetableToTime(found.timetable)
              const from = new Date(now.getFullYear(), now.getMonth(), now.getDate(), interval.from.h, interval.from.m)
              const to = new Date(now.getFullYear(), now.getMonth(), now.getDate(), interval.to.h, interval.to.m)
              if (now >= from && now <= to) {
                this.invalidAccessCode = false
                this.exam.init(this.enrollmentId, classId, config)
                this.router.navigate(['/rules'])
              }
            }
          }
          this.invalidAccessCode = true
        })
    }
  }

}
