import { Component, OnInit } from '@angular/core';
import { ExamService } from '../exam/exam.service';
import { Router } from '@angular/router';

/**
 * Final page, after the exam finishes.
 */
@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {

  constructor(public exam: ExamService, private router: Router) {
  }

  ngOnInit() {
    if (!this.exam.isInitialized()) {
      this.router.navigate(['/'])
    }
    this.exam.finishExam()
  }

}
