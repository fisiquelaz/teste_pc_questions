import { Pipe, PipeTransform } from '@angular/core';

/**
 * Pipe to transform a time in seconds to the format hh:mm:ss.
 */
@Pipe({
  name: 'time'
})
export class TimePipe implements PipeTransform {

  transform(value: number): string {
    const hours = Math.floor(value / 3600)
    const secondsLeft = value - hours * 3600
    const minutes = Math.floor(secondsLeft / 60)
    const seconds = Math.floor(secondsLeft - minutes * 60)
    const hStr = hours.toString().padStart(2, '0')
    const mStr = minutes.toString().padStart(2, '0')
    const sStr = seconds.toString().padStart(2, '0')
    return `${hStr}:${mStr}:${sStr}`
  }

}
