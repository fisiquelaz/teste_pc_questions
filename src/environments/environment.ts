// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDCG_TQevvGpzMQnJEA_SmLH4DUoyoTd_c',
    authDomain: 'pens-comp.firebaseapp.com',
    databaseURL: 'https://pens-comp.firebaseio.com',
    projectId: 'pens-comp',
    storageBucket: 'pens-comp.appspot.com',
    messagingSenderId: '1025794221156'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
